<!DOCTYPE html>

<?php
	$flag = 0;
	if (isset($_POST))
		if (isset($_POST['pseudo']) && isset($_POST['age']) && isset($_POST['pays']))
			$flag = 1;
?>

<html lang="fr">
    <head>
        <title>La page douce</title>
        <meta charset='utf-8' />
        <link rel="stylesheet" type="text/css" href="style.css"/>
        <link rel="stylesheet" type="text/css" href="simplegrid.css"/>
    </head>

    <body>
    	<article class="col-1-1">
            <header>
                <H1>Page de binvenue !</H1>
            </header>
            <section class="col-1-1">
	            <h2>
			    	<?php if($flag == 1) {
			    		$pseudo = htmlspecialchars($_POST['pseudo']);
			    		switch ($_POST['pays']) {
			    			case 'chine':
					    		if($_POST['age'] < 0)
					    			echo '你好小' . $pseudo;
					    		else
					    			echo '你好' . $pseudo;
					    	break;
					    	case 'royaume-uni':
					    	case 'canada':
					    	case 'etats-unis':
					    		if($_POST['age'] < 0)
					    			echo 'Hi little ' . $pseudo;
					    		else
					    			echo 'Hi ' . $pseudo;
					    	break;
					    	case 'france':
					    		if($_POST['age'] < 0)
					    			echo 'Salut petit ' . $pseudo;
					    		else
					    			echo 'Salut ' . $pseudo;
					    	break;
				    	}
			    	}
			    	?>
			    </h2>
			    <p>Nous t'accueillons aujord'hui dans notre humble demeure de chamallow !</p>
			    <p>Soit heureux & vie en paix avec tes nouveaux amis chamallow !</p>
			</section>
			<section class="col-1-1">
    			<a href="index.php">Retour à l'index</a>
    		</section>
    	</article>
    </body>
</html>
