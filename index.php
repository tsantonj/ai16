<!DOCTYPE html>
<html lang="fr">
    <body>
        <?php include('_header.php'); ?>

        <article class="col-1-1">
            <header>
                <H1>TP1: Hello World!</H1>
            </header>

            <div class="col-1-2">
            <section class="col-1-1">
                <h2>3. Manipulation de variables</h2>
                <?php
                    $var1 = 'Hello ';
                    echo $var1;
                    $var2 = 'Santonja';
                    $var2 .= $var1;
                    echo $var2;
                ?>
            </section>
            
            </hr>

            <section class="col-1-1">
                <h2>4. Boucles</h2>
                <?php
                    for ($i = 1; $i <= 10; $i++)
                    {
                        echo 'Boucle deterministe, itération n°' . $i . '<br/>';
                    }
                    $i = 1;
                    while ($i <= 10)
                    {
                        echo 'Boucle indeterministe, itération n°' . $i . '<br/>';
                        $i++;
                    }
                ?>
            </section>
            </div>

            </hr>

            <section class="col-1-2">
                <h2>5. Conditions</h2>
                <?php
                    for ($age = 0; $age <= 25; $age++)
                    {
                        $age++;
                        echo 'Si tu as ' . $age . ' avec une condition si : ' . '<br/>';
                        if ($age <= 12)
                        {
                            echo 'Salut gamins !';
                        }
                        else if ($age <= 18)
                        {
                            echo 'Salut jeune homme/fille !';
                        }
                        else
                        {
                            echo 'Salut jeune padawan !';
                        }
                        echo 'Si tu as ' . $age . ' avec une switch case : ' . '<br/>';
                        switch ($age) {
                            case 0:
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                            case 6:
                            case 7:
                            case 8:
                            case 9:
                            case 10:
                            case 11:
                            case 12:
                                echo 'Salut gamins !';
                            break;
                    
                            case 13:
                            case 14:
                            case 15:
                            case 16:
                            case 17:
                            case 18:
                                echo 'Salut jeune homme/fille !';
                            break;
                                    
                            default:
                                echo 'Salut jeune padawan !';
                            break;
                        }
                        echo '<br/>';
                    }
                ?>
            </section>
        </article>

        <hr>

        <article class="col-1-1">
            <header>
                <H1>TP2: PHP et les tableaux</H1>
            </header>

            <section>
                <?php include('data.php'); ?>
                <?php
                    for ($i = 0; $i < count($arr_persos1); $i++)
                    {
                        echo $arr_persos1[$i] . '<br/>';
                    }
                    foreach ($arr_persos2 as $value) {
                        echo $value . '<br/>';
                    }

                    foreach ($arr_persos3 as $perso) 
                    {
                        echo '<dl>';
                        foreach ($perso as $dt => $dd) 
                        {
                            echo '<dt>' . $dt . '</dt>';
                            echo '<dd>' . $dd . '</dd>';
                        }
                        echo '</dl>';
                    }
                    echo rand(-10, 10);
                ?>
            </section>
        </article>

        <hr>

        <article class="col-1-1">
            <header>
                <H1>TP3: PHP et les les fonctions</H1>
            </header>

            <section>
                <?php include('functions.php'); ?>
                <?php 
                    echo sayHello('Ben Kenobi'); 
                ?>

                <?php
                    $coucou = 'Hey, wake up !';
                    echo $coucou . ' : ' . strlen($coucou);
                ?>
                  
                <hr>

                <?php
                    echo $coucou . ' : ' . str_replace(" ", "_", $coucou);
                ?>
                  
                <hr>

                <?php
                    echo $coucou . ' : ' . str_shuffle($coucou);
                ?>

                <hr>

                <?php
                    echo '<ul>';
                    echo '<li>' . $coucou . ' : ' . strtoupper($coucou) . '</li>';
                    echo '<li>' . $coucou . ' : ' . strtolower($coucou) . '</li>';
                    echo '</ul>';
                ?>
            </section>
        </article>

        <hr>

        <article class="col-1-1">
            <header>
                <H1>TP4: PHP et HTML</H1>
            </header>

            <section>
                <?php

                ?>
            </section>
        </article>

        <hr>

        <article class="col-1-1">
            <header>
                <H1>TP5: Securirte</H1>
            </header>

            <section>
            <?php

            ?>
            </section>
        </article>

        <hr>

        <article class="col-1-1">
            <header>
                <H1>TP6: PHP & les données d'URL</H1>
            </header>

            <section>
                <?php

                ?>
            </section>
        </article>

        <hr>

        <article class="col-1-1">
            <header>
                <H1>TP7: PHP & formulaires</H1>
            </header>

            <section>
                <form action="helloPage.php" method="POST">
                    Saisissez votre pseudo : 
                    <input type="text" name="pseudo" value="Pseudo"/>
                    <p>Veuillez indiquer la tranche d'âge dans laquelle vous vous situez :<br />
                        <input type="radio" name="age" value="-18" id="moins18" /> <label for="-18">Moins de 18 ans</label><br/>
                        <input type="radio" name="age" value="+18" id="plus18" /> <label for="+18">plus de 18ans</label><br/>
                        <select name="pays" id="pays">
                            <option value="france">France</option>
                            <option value="royaume-uni">Royaume-Uni</option>
                            <option value="canada">Canada</option>
                            <option value="etats-unis">États-Unis</option>
                            <option value="chine">Chine</option>
                        </select>
                    </p>
                    <input type="submit" value="Send" />
                </form>
            </section>
        </article>

        <hr>

        <article class="col-1-1">
            <header>
                <H1>TP8: PHP & securité 2</H1>
            </header>

            <section>
                <?php

                ?>
            </section>
        </article>

        <hr>

        <article class="col-1-1">
            <header>
                <H1>TP42: Test</H1>
            </header>

            <section class="tp42">
                <h2>Lien vers le site du TP</h2>
                <a href="../2016-11-22 tp 42 php/index.php" target="_blank">Cliquez-ici</a>
            </section>
        </article>

        <hr>

        <article class="col-1-1">
            <header>
                <H1>Autre</H1>
            </header>

            <section class="tp42">
                <h2>Lien vers le site de palettes de couleurs</h2>
                <a href="http://colrd.com/swatch=17B6FF,FF17B6,B6FF17,1BFF17" target="_blank">Cliquez-ici</a>
            </section>

            <section class="tp42">
                <h2>à voir</h2>
                <ul>
                    <li>AngularJS</li>
                    <li>AJAX</li>
                    <li>LESS</li>
                </ul>
            </section>
        </article>
    </body>
</html>
<!--    http://trazafin.alwaysdata.net/transfert/Web_TP-PHP/    -->